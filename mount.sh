#!/bin/sh

set -e

crypt_open() {
  echo "opening $1 as $2..."
  cryptsetup open "$1" "$2"
}

mounted() {
  grep -qs "$1 " "/proc/mounts"
}

mount_crypt() {
  echo "mounting subvolume $2 of $1 as $3..."
  mount -o "compress=zstd,discard=async,noatime,subvol=$2" "$1" "$3"
}

mount_crypt_cond() {
  if ! mounted "$3"; then
    mount_crypt "$1" "$2" "$3"
  fi
}

mount_cond() {
  if ! mounted "$2"; then
    mount "$1" "$2"
  fi
}

if [ "$EUID" -ne 0 ]; then
  echo "run as root"
  exit 1
fi

CRYPT_DEVICE="/dev/disk/by-partlabel/NIXCRYPT"
CRYPT_DISK_NAME="nix-crypt"
CRYPT_DISK="/dev/mapper/$CRYPT_DISK_NAME"

if [ ! -b "$CRYPT_DISK" ]; then
  crypt_open "$CRYPT_DEVICE" "$CRYPT_DISK_NAME"
fi

mount_crypt_cond "/dev/mapper/nix-crypt" "@root" "/mnt"
mount_crypt_cond "/dev/mapper/nix-crypt" "@roothome" "/mnt/root"
mount_crypt_cond "/dev/mapper/nix-crypt" "@home" "/mnt/home"
mount_crypt_cond "/dev/mapper/nix-crypt" "@var" "/mnt/var"
mount_crypt_cond "/dev/mapper/nix-crypt" "@var-lib" "/mnt/var/lib"
mount_crypt_cond "/dev/mapper/nix-crypt" "@var-log" "/mnt/var/log"
mount_crypt_cond "/dev/mapper/nix-crypt" "@var" "/mnt/var"
mount_crypt_cond "/dev/mapper/nix-crypt" "@nix" "/mnt/nix"

mount_cond "/dev/disk/by-partlabel/NIXBOOT" "/mnt/boot"
