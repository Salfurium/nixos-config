{ config, pkgs, ... }:

with import <nixpkgs> { };
with builtins;
{
  home-manager.users.david = {
    # Home Manager needs a bit of information about you and the
    # paths it should manage.
    home.username = "david";
    home.homeDirectory = "/home/david";

    # This value determines the Home Manager release that your
    # configuration is compatible with. This helps avoid breakage
    # when a new Home Manager release introduces backwards
    # incompatible changes.
    #
    # You can update Home Manager without changing this value. See
    # the Home Manager release notes for a list of state version
    # changes in each release.
    home.stateVersion = "21.11";

    home.packages = with pkgs; [
      htop
      btop
      neovim
      gnome.gnome-software
      gnome.gnome-tweaks
      gnome.gnome-shell-extensions
      flatpak
      tela-icon-theme
      firefox
      vscode
      bitwarden
    ];

    # Let Home Manager install and manage itself.
    programs.home-manager.enable = true;

    xdg = {
      enable = true;
      mime.enable = true;
    };

    home.sessionVariables = {
      EDITOR = "nvim";
      VISUAL = "gedit";
    };

    programs.git = {
      enable = true;
      userName = "David Neugebauer";
      userEmail = "example@example.com";
      extraConfig = {
        pull.ff = "only";
      };
    };

    programs.bash.enable = true;
    programs.zsh = {
      enable = true;
      shellAliases = {
        "vim" = "nvim";
      };
      profileExtra = "source ~/.profile";
      enableSyntaxHighlighting = true;
      oh-my-zsh = {
        enable = true;
        theme = "gallois";
        plugins = [ "git" ];
      };
    };

    programs.ssh = {
      enable = true;
      matchBlocks."gitlab.com" = {
        user = "git";
        hostname = "gitlab.com";
        identityFile = "~/.ssh/id_ed25519";
      };
    };
  };
}
