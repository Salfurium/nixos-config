{ config, lib, pkgs, modulesPath, ... }:
{
  imports = [
    ./home.nix
  ];

  users.users.david = {
    description = "David Neugebauer";
    initialHashedPassword = "$6$.b63ap7hGLEjESUG$FuM4lkS59RyR5pyZuH3V/IvB7m.HgvRPCEuox4soE95BrHmv3Hm/3ft5XQV3FXHMae0J7TmUPZ7R3WylBuRlD1";
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" ];
    shell = pkgs.zsh;
    openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPAfWtc9oUnn48/qsGz46MWHB/e0Q1nlMBnw/ZnlXnDd david@David-PC" ];
    subUidRanges = [{ startUid = 100000; count = 65536; }];
    subGidRanges = [{ startGid = 100000; count = 65536; }];
  };
}
