#!/bin/sh

set -e

usage() {
  echo "run as root: ./install.sh [SYTEM ROOT DIR]"
  echo "[SYTEM ROOT DIR] defaults to /"
  echo "will install config to [SYTEM ROOT DIR]/etc/nixos"
}

replace_email() {
  local EMAIL_PLACEHOLDER="example@example.com"
  local EMAIL
  if [ ! -f "$HOME/.config/email" ]; then
    echo "type your email address:"
    read EMAIL
    mkdir -p "$HOME/.config"
    echo "$EMAIL" > "$HOME/.config/email"
  else
    EMAIL="$(cat "$HOME/.config/email")"
  fi

  local REPLACE_OP="s/$EMAIL_PLACEHOLDER/$EMAIL/g"

  find "$INSTALL_TARGET" -type f -name "*.nix" -exec sed -i "$REPLACE_OP" {} \;
}

if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
  usage
  exit 0
fi

if [ "$EUID" -ne 0 ]; then
  echo "needs to be run as root"
  usage
  exit 1
fi

INSTALL_ROOT="$1"
if [ "$INSTALL_ROOT" = "" ]; then
  INSTALL_ROOT="/"
fi

if [ ! -d "$INSTALL_ROOT" ];then
  echo "$INSTALL_ROOT is not a directory."
  usage
  exit 1
fi

export INSTALL_TARGET="$INSTALL_ROOT/etc/nixos"

find ./ -name '*.nix' -exec cp --parents {} "$INSTALL_TARGET" ';'
replace_email
