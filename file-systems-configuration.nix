{ config, lib, pkgs, modulesPath, ... }:

{
  boot.initrd.luks.devices."nix-crypt".device = "/dev/disk/by-partlabel/NIXCRYPT";

  fileSystems."/" =
    {
      device = "/dev/mapper/nix-crypt";
      fsType = "btrfs";
      options = [ "noatime" "compress=zstd" "discard=async" "subvol=@root" ];
    };

  fileSystems."/root" =
    {
      device = "/dev/mapper/nix-crypt";
      fsType = "btrfs";
      options = [ "noatime" "compress=zstd" "discard=async" "subvol=@roothome" ];
    };

  fileSystems."/var" =
    {
      device = "/dev/mapper/nix-crypt";
      fsType = "btrfs";
      options = [ "noatime" "compress=zstd" "discard=async" "subvol=@var" ];
    };

  fileSystems."/var/log" =
    {
      device = "/dev/mapper/nix-crypt";
      fsType = "btrfs";
      options = [ "noatime" "compress=zstd" "discard=async" "subvol=@var-log" ];
    };

  fileSystems."/var/lib" =
    {
      device = "/dev/mapper/nix-crypt";
      fsType = "btrfs";
      options = [ "noatime" "compress=zstd" "discard=async" "subvol=@var-lib" ];
    };

  fileSystems."/home" =
    {
      device = "/dev/mapper/nix-crypt";
      fsType = "btrfs";
      options = [ "noatime" "compress=zstd" "discard=async" "subvol=@home" ];
    };

  fileSystems."/nix" =
    {
      device = "/dev/mapper/nix-crypt";
      fsType = "btrfs";
      options = [ "noatime" "compress=zstd" "discard=async" "subvol=@nix" ];
    };

  fileSystems."/boot" =
    {
      device = "/dev/disk/by-partlabel/NIXBOOT";
      fsType = "vfat";
    };

  swapDevices =
    [
      {
        device = "/dev/disk/by-partlabel/NIXSWAP";
        randomEncryption = {
          enable = true;
          cipher = "aes-xts-plain64";
          source = "/dev/urandom";
        };
      }
    ];
}

